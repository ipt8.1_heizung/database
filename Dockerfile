FROM mongo:latest

# Setzen Sie die Umgebungsvariable für die initiale Datenbank
ENV MONGO_INITDB_DATABASE=IPT8.1Heater

# Fügen Sie das Initialisierungsskript hinzu
COPY init-mongo.py /docker-entrypoint-initdb.d/

# Der Standardport von MongoDB
EXPOSE 27017
