import pymongo

client = pymongo.MongoClient("mongodb://localhost:27017/")

db = client["IPT8.1Heater"]

db.create_collection("week-schedule")
db.create_collection("component-collection")
db.create_collection("settings")
db.create_collection("io-collection")

client.close()
